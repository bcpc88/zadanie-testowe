<?php

namespace App\Controller;

class InvoicesController extends AppController
{
	public function index()
    {
        $this->loadComponent('Paginator');
        $invoices = $this->Paginator->paginate($this->Invoices->find());
        $this->set(compact('invoices'));
    }

	public function ajax()
    {	
		$this->viewBuilder()->layout("ajax");
        //$this->loadComponent('Paginator');
        $invoices = $this->Invoices->find();
		$tmp = array("data" => array());
		
		foreach($invoices as $i) {
			$tmp['data'][] = array(
				$i->nb, 
				$i->date_added->format(DATE_RFC850),
				$i->price_all,
				$i->company,
				$i->email,
				$i->address,
			);
		}
		
		echo json_encode($tmp);
		exit();
    }
}