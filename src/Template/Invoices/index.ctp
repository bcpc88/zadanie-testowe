<div class="row">
	<div class="col-xs-12">

	  <div class="box">
		<div class="box-header">
		  <h3 class="box-title">Data Table With Full Features</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="invoices" class="table table-bordered table-striped">
			<thead>
			<tr>
				<th>Numbers</th>
				<th>Created</th>
				<th>Total price</th>
				<th>Company name</th>
				<th>Address</th>
				<th>E-mail</th>
			</tr>
			</thead>
			<tbody>
			
			</tbody>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
	  <!-- /.box -->
	</div>
	<!-- /.col -->
  </div>
  <!-- /.row -->